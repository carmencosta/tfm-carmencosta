#Para ejecutar este programa usar: alignment_script.sh 

cd /DATA/workspace/ccg/data_carmen/
sudo mkdir -p bams
sudo chmod 777 -R bams

cd /DATA/workspace/ccg/data_carmen/trimmed
for sample_name in /DATA/workspace/ccg/data_carmen/trimmed/*_L1_1_paired.trim.fq.gz 
do 
   base=$(basename ${sample_name} _L1_1_paired.trim.fq.gz)
   echo $base
  
   bwa mem -t4 -M -R "@RG\tID:${base}\tPL:ILLUMINA\tSM:${base}\tDS:ref=37d5\tCN:UGDG\tDT:2021_11_03\tPU:${base}" /DATA/workspace/ccg/hs37d5.fa /DATA/workspace/ccg/data_carmen/trimmed/${base}_L1_1_paired.trim.fq.gz /DATA/workspace/ccg/data_carmen/trimmed/${base}_L1_2_paired.trim.fq.gz | samtools view -S -b - > /DATA/workspace/ccg/data_carmen/bams/Exome_Exomev1_${base}.bam
done

for sample_name in /DATA/workspace/ccg/data_carmen/trimmed/*_R1_paired.trim.fq.gz 
do 
  base=$(basename ${sample_name} _R1_paired.trim.fq.gz)
  echo $base
  bwa mem -t4 -M -R "@RG\tID:${base}\tPL:ILLUMINA\tSM:${base}\tDS:ref=37d5\tCN:UGDG\tDT:2021-11-03\tPU:${base}" /DATA/workspace/ccg/hs37d5.fa /DATA/workspace/ccg/data_carmen/trimmed/${base}_R1_paired.trim.fq.gz /DATA/workspace/ccg/data_carmen/trimmed/${base}_R2_paired.trim.fq.gz | samtools view -S -b - > /DATA/workspace/ccg/data_carmen/bams/agilent_v5_${base}.bam 
done

for sample_name in /DATA/workspace/ccg/data_carmen/trimmed/*_R1_001_paired.trim.fq.gz 
do 
  base=$(basename ${sample_name} _R1_001_paired.trim.fq.gz )
  echo $base
  bwa mem -t4 -M -R "@RG\tID:${base}\tPL:ILLUMINA\tSM:${base}\tDS:ref=37d5\tCN:UGDG\tDT:2021-11-03\tPU:${base}" /DATA/workspace/ccg/hs37d5.fa /DATA/workspace/ccg/data_carmen/trimmed/${base}_R1_001_paired.trim.fq.gz /DATA/workspace/ccg/data_carmen/trimmed/${base}_R2_001_paired.trim.fq.gz | samtools view -S -b - > /DATA/workspace/ccg/data_carmen/bams/formatted_truseq_${base}.bam 
done

# Ejecutar programa ngscat2

#Coger las lecturas mapeadas
cd /DATA/workspace/ccg/data_carmen/bams
for sample_name in /DATA/workspace/ccg/data_carmen/bams/*.bam
do 
  base=$(basename ${sample_name} .bam)
  echo $base

  sudo samtools view -b -F 4 /DATA/workspace/ccg/data_carmen/bams/${base}.bam > ${base}.mapped.bam 

#Ordenarlos
  sudo samtools sort ${base}.mapped.bam -o ${base}.sorted.bam

#Indexarlos

  sudo samtools index ${base}.sorted.bam
done
# Correr ngscat2

cd /DATA/workspace/ccg/data_carmen/
sudo mkdir -p ngscat2
sudo chmod 777 -R ngscat2

cd /DATA/workspace/ccg/data_carmen/bams
for sample_name in /DATA/workspace/ccg/data_carmen/bams/Exome_Exomev1*.sorted.bam
do
  base=$(basename ${sample_name} .sorted.bam)
  echo $base
  ngscat2 --bams ${base}.sorted.bam --bed /DATA/workspace/ccg/coverage_analysis/Exome_Exomev1_Probes.hg19.manifest.bed  --reference /DATA/workspace/ccg/hs37d5.fa --out /DATA/workspace/ccg/data_carmen/ngscat2/${base}

done


for sample_name in /DATA/workspace/ccg/data_carmen/bams/agilent_v5*.sorted.bam
do
  base=$(basename ${sample_name} .sorted.bam)
  echo $base
  ngscat2 --bams ${base}.sorted.bam --bed /DATA/workspace/ccg/coverage_analysis/agilent_v5_nochr_formatted.bed --reference /DATA/workspace/ccg/hs37d5.fa --out /DATA/workspace/ccg/data_carmen/ngscat2/${base}

done


for sample_name in /DATA/workspace/ccg/data_carmen/bams/formatted_truseq*.sorted.bam
do
  base=$(basename ${sample_name} .sorted.bam)
  echo $base
  ngscat2 --bams ${base}.sorted.bam --bed /DATA/workspace/ccg/coverage_analysis/formatted_truseq_exome_targeted_regions.hg19.nochr.bed --reference /DATA/workspace/ccg/hs37d5.fa --out /DATA/workspace/ccg/data_carmen/ngscat2/${base}

done

