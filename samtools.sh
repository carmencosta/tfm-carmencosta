cd /DATA/workspace/ccg/data_carmen/bams


for sample_name in /DATA/workspace/ccg/data_carmen/bams/*.sorted.bam
do
   base=$(basename ${sample_name} .sorted.bam)
   echo $base

 samtools rmdup -S ${base}.sorted.bam ${base}.rmdup.bam
 samtools flagstat ${base}.bam --threads 5 >> ${base}.flagstat.txt
 samstat ${base}.sorted.bam
done


