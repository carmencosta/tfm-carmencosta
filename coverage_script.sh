sudo python bed_files.py
 
cd /DATA/workspace/ccg/data_carmen/
sudo mkdir -p coverage
sudo chmod 777 -R coverage


sudo bedtools intersect -a /DATA/workspace/ccg/data_carmen/bams/Exome_Exomev1_OCTVA-235-UKKD19070049-AK1583-AK413_HGWJMDSXX.bam -b /DATA/workspace/ccg/coverage_analysis/Exome_Exomev1_Probes.hg19.manifest.bed > /DATA/workspace/ccg/data_carmen/bams/Exome_Exomev1_OCTVA-235-UKKD19070049-AK1583-AK413_HGWJMDSXX.bam

sudo bedtools intersect -a /DATA/workspace/ccg/data_carmen/bams/agilent_v5_06-140373-P7i5-P5i20.bam -b /DATA/workspace/ccg/coverage_analysis/Exome_Exomev1_Probes.hg19.manifest.bed > /DATA/workspace/ccg/data_carmen/bams/agilent_v5_06-140373-P7i5-P5i20.bam

for sample_name in /DATA/workspace/ccg/data_carmen/bams/Exome_Exomev1_*.bam
do
  base=$(basename ${sample_name} .bam)
  echo $base

  sudo bedtools coverage -d -a /DATA/workspace/ccg/coverage_analysis/Exome_Exomev1_Targets.hg19.manifest.bed -b /DATA/workspace/ccg/data_carmen/bams/${base}.bam > /DATA/workspace/ccg/data_carmen/coverage/Exome_Exomev1_${base}.bedtools.cov

done

for sample_name in /DATA/workspace/ccg/data_carmen/bams/formatted_truseq*.bam
do
  base=$(basename ${sample_name} .bam)
  echo $base

  sudo bedtools coverage -d -a /DATA/workspace/ccg/coverage_analysis/formatted_truseq_exome_targeted_regions.hg19.nochr.bed -b /DATA/workspace/ccg/data_carmen/bams/${base}.bam > /DATA/workspace/ccg/data_carmen/coverage/formatted_truseq${base}.bedtools.cov

done


for sample_name in /DATA/workspace/ccg/data_carmen/bams/agilent_v5*.bam
do
  base=$(basename ${sample_name} .bam)
  echo $base

  sudo bedtools coverage -d -a /DATA/workspace/ccg/coverage_analysis/agilent_v5_nochr_formatted.bed -b /DATA/workspace/ccg/data_carmen/bams/${base}.bam > /DATA/workspace/ccg/data_carmen/coverage/agilent_v5${base}.bedtools.cov

done


#Crear carpeta de images para guardar las imagenes del coverage.py
cd /DATA/workspace/ccg/data_carmen/
sudo mkdir -p images
sudo chmod 777 images

sudo python coverage.py
