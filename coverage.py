import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

#Cargar datos
coverage = pd.read_csv("/DATA/workspace/ccg/coverage/Exome_Exomev1_OCTVA-235-UKKD19070049-AK1583-AK413_HGWJMDSXX.bedtools.cov", sep='\t', header=None, index_col=False, low_memory=False, names= ["chrom", "chromStart", "chromEnd", "gene_name", "n", "cov"])

coverage2 = pd.read_csv ("/DATA/workspace/ccg/data_carmen/coverage/Exome_Exomev1_S653_UKKD19070049-AK366-AK417_HGWJMDSXX.bedtools.cov", sep='\t', header=None, index_col=False, low_memory=False, names= ["chrom", "chromStart", "chromEnd", "gene_name", "n", "cov"])

coverage
coverage2

# Coger sólo un gen de la columna gene_name (OCTVA)
coverage[coverage["gene_name"] == 'OR4F5']

#Boxplot representando la cobertura del gen OR4F5
sns.boxplot(data=coverage[coverage["gene_name"] == 'OR4F5'], y="cov")
plt.savefig("/DATA/workspace/ccg/data_carmen/images/gen_OR4F5_OCTVA.png", transparent=True)

#Boxplot representando la cobertura del gen OR4F5 (S653)
sns.boxplot(data=coverage[coverage["gene_name"] == 'OR4F5'], y="cov")
plt.savefig("/DATA/workspace/ccg/images/gen_OR4F5_S653.png", transparent=True)


#Dar una lista de genes concreta y representarlos según la cobertura
genes_interest= ['TCEB3CL', 'NPIPB4', 'VCX3A', 'GAGE2B', 'NPIPB5', 'TBC1D3H', 'USP17L28', 'KRTAPA12']
sns.violinplot(data=coverage.loc[coverage.gene_name.isin(genes_interest)], x="gene_name", y="cov")
plt.savefig("/DATA/workspace/ccg/images/lista_genes_OCTVA.png", transparent=True)


#Dar una lista de genes concreta y representarlos según la cobertura
genes_interest= ['TCEB3CL', 'NPIPB4', 'VCX3A', 'GAGE2B', 'NPIPB5', 'TBC1D3H', 'USP17L28', 'KRTAPA12']
sns.violinplot(data=coverage2.loc[coverage.gene_name.isin(genes_interest)], x="gene_name", y="cov")
plt.savefig("/DATA/workspace/ccg/images/lista_genes_S653.png", transparent=True)

#Calcular la media

coverage["cov"].mean()
coverage2["cov"].mean()

# How many individuals are there:
coverage['gene_name'].unique()
coverage2['gene_name'].unique()

#Seleccionar únicamente esa lista de genes del dataframe
coverage.loc[coverage.gene_name.isin(genes_interest)]
coverage2.loc[coverage2.gene_name.isin(genes_interest)]


#Listado de genes que tengan una cobertura menor a 10 y representarlo
coso = coverage.groupby("gene_name")["cov"].mean().reset_index()
coso.loc[coso["cov"] <= 10]
sns.violinplot(data=coso.loc[coso["cov"] <= 10], x="gene_name", y="cov")
plt.savefig("/DATA/workspace/ccg/images/lista_genes_OC_menor_a_10.png", transparent=True)

copo = coverage2.groupby("gene_name")["cov"].mean().reset_index()
copo.loc[copo["cov"] <= 10]
sns.violinplot(data=copo.loc[copo["cov"] <= 10], x="gene_name", y="cov")
plt.savefig("/DATA/workspace/ccg/images/lista_genes_S653_menor_a_10.png", transparent=True)
