cd /DATA/workspace/ccg/data_carmen/

sudo mkdir -p variant_calling
sudo chmod 777 variant_calling


bcftools mpileup -Ou -f /DATA/workspace/ccg/hs37d5.fa /DATA/workspace/ccg/data_carmen/bams/*.rmdup.bam | bcftools call -vmO z -o /DATA/workspace/ccg/data_carmen/variant_calling/full_samples_rawcalls.vcf.gz
bcftools index /DATA/workspace/ccg/data_carmen/variant_calling/full_samples_rawcalls.vcf.gz
bcftools filter -i 'QUAL>10&&DP>10' /DATA/workspace/ccg/data_carmen/variant_calling/full_samples_rawcalls.vcf.gz -o /DATA/workspace/ccg/data_carmen/variant_calling/full_samples_rawcalls.vcf.gz_filter_all.vcf.gz 


