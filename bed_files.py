import pandas as pd
probes = pd.read_csv("/DATA/workspace/ccg/metadata/Exome_Exomev1_Probes.hg19.manifest.txt", sep='\t', header=None, names= ["genes", "chrom", "chromStart", "chromEnd", "4", "5"])
probes
probes = probes.drop(columns= ['4','5'])
probes["gene_name"] = probes.genes.apply(lambda x:x.split("(")[1].split(")")[0])
probes
probes = probes.drop(columns= ['genes'])
probes["chrom"] = probes.chrom.apply(lambda x:x.split("chr")[1])
probes.to_csv("/DATA/workspace/ccg/coverage_analysis/Exome_Exomev1_Probes.hg19.manifest.bed", sep="\t", header=False, index=False)

targets = pd.read_csv("/DATA/workspace/ccg/metadata/Exome_Exomev1_Targets.hg19.manifest.txt", sep='\t', header=None, names= ["genes", "chrom", "chromStart", "chromEnd", "4", "5"])
targets
targets = targets.drop(columns= ['4','5'])
targets["gene_name"] = targets.genes.apply(lambda x:x.split("(")[1].split(")")[0])
targets = targets.drop(columns= ['genes'])
targets["chrom"] = targets.chrom.apply(lambda x:x.split("chr")[1])
targets
targets.to_csv("/DATA/workspace/ccg/coverage_analysis/Exome_Exomev1_Targets.hg19.manifest.bed", sep="\t", header=False, index=False)

agilent = pd.read_csv("/DATA/workspace/ccg/agilent_v5_nochr_formatted.bed", sep='\t', header=None, names= ["chrom", "chromStart", "chromEnd"])
agilent.to_csv("/DATA/workspace/ccg/coverage_analysis/agilent_v5_nochr_formatted.bed", sep="\t", header=False, index=False)


truseq = pd.read_csv("/DATA/workspace/ccg/formatted_truseq_exome_targeted_regions.hg19.nochr.bed", sep='\t', header=None, names= ["chrom", "chromStart", "chromEnd"])
truseq.to_csv("/DATA/workspace/ccg/coverage_analysis/formatted_truseq_exome_targeted_regions.hg19.nochr.bed", sep="\t", header=False, index=False)
