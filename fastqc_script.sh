#Para ejecutar este programa usar: sh fastqc_script.sh &

##Programa 1 en bash - fastqc_script.sh

#Este script permite comprobar la calidad de las muestras a partir del programa FASTQC
cd /DATA/workspace/ccg/data_carmen/
sudo mkdir -p fastqc
sudo chmod 777 fastqc
ruta="/DATA/workspace/ccg/data_carmen/exome_test_samples"
for file in $(ls $ruta)
do
        fastqc $ruta/$file -o /DATA/workspace/ccg/data_carmen/fastqc    ##$file es para que me coja uno a uno cada archivo que                                                  ##hay en esta carpeta
done

cd /DATA/workspace/ccg/data_carmen/fastqc
multiqc .
