
cd /DATA/workspace/ccg/data_carmen/
sudo mkdir -p trimmed
sudo chmod 777 -R trimmed


for infile in /DATA/workspace/ccg/data_carmen/exome_test_samples/*_L1_1.fq.gz
do
  base=$(basename ${infile} _L1_1.fq.gz)
  echo $base
  java -jar /home/ccg/trimmomatic-0.39.jar PE -phred33 ${infile} /DATA/workspace/ccg/data_carmen/exome_test_samples/${base}_L1_2.fq.gz \
               trimmed/${base}_L1_1_paired.trim.fq.gz trimmed/${base}_L1_1_unpaired.trim.fq.gz \
               trimmed/${base}_L1_2_paired.trim.fq.gz trimmed/${base}_L1_2_unpaired.trim.fq.gz \
               ILLUMINACLIP:/home/ccg/NexteraPE-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36  
done


for infile in /DATA/workspace/ccg/data_carmen/exome_test_samples/*_R1_001.fastq.gz
do
  base=$(basename ${infile} _R1_001.fastq.gz)
  echo $base
  java -jar /home/ccg/trimmomatic-0.39.jar PE -phred33 ${infile} /DATA/workspace/ccg/data_carmen/exome_test_samples/${base}_R2_001.fastq.gz \
               trimmed/${base}_R1_001_paired.trim.fastq.gz trimmed/${base}_R1_001_unpaired.trim.fastq.gz \
               trimmed/${base}_R2_001_paired.trim.fastq.gz trimmed/${base}_R2_001_unpaired.trim.fastq.gz \
               LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
done

for infile in /DATA/workspace/ccg/data_carmen/exome_test_samples/*_R1.fastq.gz
do
  base=$(basename ${infile} _R1.fastq.gz)
  echo $base
  java -jar /home/ccg/trimmomatic-0.39.jar PE -phred33 ${infile} /DATA/workspace/ccg/data_carmen/exome_test_samples/${base}_R2.fastq.gz \
               trimmed/${base}_R1_paired.trim.fastq.gz trimmed/${base}_R1_unpaired.trim.fastq.gz \
               trimmed/${base}_R2_paired.trim.fastq.gz trimmed/${base}_R2_unpaired.trim.fastq.gz \
               LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
done


