
#Este script permite comprobar la calidad de las muestras a partir del programa FASTQC
cd /DATA/workspace/ccg/data_carmen/trimmed
sudo mkdir -p fastqc
sudo chmod 777 fastqc
ruta="/DATA/workspace/ccg/data_carmen/trimmed"
for file in $(ls $ruta)
do
        fastqc $ruta/$file -o /DATA/workspace/ccg/data_carmen/trimmed/fastqc    ##$file es para que me coja uno a uno cada archivo que            >
done

cd /DATA/workspace/ccg/data_carmen/trimmed/fastqc
multiqc .
